import { createStore } from "redux";
import rootReducer from "./reducers/rootR";

export default () => {
  return createStore(rootReducer);
};
