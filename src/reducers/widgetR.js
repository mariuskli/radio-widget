import { SET_ACTIVE_STATION } from '../actions/action-types';

export const INITIAL_STATE = {
  stations: [
    {
      name: "Putin FM",
      freq: "66,6"
    },
    {
      name: "Dribbble FM",
      freq: "101,2"
    },
    {
      name: "Doge FM",
      freq: "99,4"
    },
    {
      name: "Ballads FM",
      freq: "87,1"
    },
    {
      name: "Maximum FM",
      freq: "142,2"
    }
  ],
  activeStation: -1
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
   case SET_ACTIVE_STATION:
    return {
     ...state,
     activeStation: state.activeStation === action.payload ? -1 : action.payload,
    };
   default:
    return state;
  }
 };