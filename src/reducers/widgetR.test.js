import reducer, { INITIAL_STATE } from './widgetR';
import * as types from '../actions/action-types';

describe('Widget reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      ...INITIAL_STATE,
      activeStation: -1
    });
  });

  it(`should handle ${types.SET_ACTIVE_STATION}`, () => {
    const payload = 1;

    expect(
      reducer(undefined, {
        type: types.SET_ACTIVE_STATION,
        payload
      })
    ).toEqual({
      ...INITIAL_STATE,
      activeStation: payload
    });

    //If same activeStation value is passed as payload, it should reset activeStation to initial value
    expect(
      reducer(
        {
          ...INITIAL_STATE,
          activeStation: payload
        },
        {
          type: types.SET_ACTIVE_STATION,
          payload
        }
      )
    ).toEqual({
      ...INITIAL_STATE,
      activeStation: -1
    });
  });
});