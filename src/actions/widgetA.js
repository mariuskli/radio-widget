import { SET_ACTIVE_STATION } from './action-types';

export const setActiveStation = (payload) => ({
  type: SET_ACTIVE_STATION,
  payload
});