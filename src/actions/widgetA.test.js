import * as actions from './widgetA';
import * as types from './action-types';

describe('Widget actions', () => {
  it('should create an action to set active station', () => {
    const payload = 1;
    const expectedAction = {
      type: types.SET_ACTIVE_STATION,
      payload
    };
    expect(actions.setActiveStation(payload)).toEqual(expectedAction);
  });
});