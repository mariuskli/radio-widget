import React from 'react';
import styled from 'styled-components';

import { BackButton, QuitButton } from './buttons';

const StyledHeader = styled.div`
  background: #eaa455;
  height: 160px;
  border-top-right-radius: 50px;
  border-top-left-radius: 50px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: 700;
  font-size: 44px;
  color: #fff;
  padding: 0 40px;
`;

const HeaderC = () => {
  return (
    <StyledHeader>
      <BackButton />
      <span>STATIONS</span>
      <QuitButton />
    </StyledHeader>
  );
};

export default HeaderC;
