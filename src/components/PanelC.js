import React from 'react';
import styled from 'styled-components';
import { Transition } from 'react-spring/renderprops';
import PropTypes from 'prop-types';
import img from '../assets/img/station.png';

import { PlusButton, MinusButton } from './buttons';

const StyledPanel = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const StyledImg = styled.img`
  border-radius: 50%;
  widht: 280px;
  height: 280px;
  max-width: 100%;
  objectFit: cover;
  border: 3px solid #a2abbd; 
`;

const PanelC = ({active}) => {

  return (
    <Transition config={{duration: 300}} items={active} from={{height: 0, opacity: 0, paddingBottom: 0}} enter={{height: 300, opacity: 1, paddingBottom: 35}} leave={{height: 0, opacity: 0, paddingBottom: 0}}>
      {active => 
        active && (
          props => (
            <StyledPanel style={props}>
              <MinusButton />
              <StyledImg src={img} alt="Station thumbnail" />
              <PlusButton />
            </StyledPanel>
          )
        )
      }
      
    </Transition>
  );
};

PanelC.propTypes = {
  active: PropTypes.bool
};

export default PanelC;