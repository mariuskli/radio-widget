import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ListItem from './ListItemC';
import { setActiveStation } from '../actions';


const StyledList = styled.ul`
  list-style: none;
  flex: 1;
  padding: 20px 40px;
  overflow-y: scroll;
  margin: 0;
`;

const renderListItems = (stations, activeIndex, onClick) => {
  return stations.map((station, index) => (
      <ListItem station={station}
                onClick={() => onClick(index)}
                key={index}
                active={activeIndex === index ? true : false}
      />
    )
  );
}

const ListC = ({stations, activeStation, setActiveStation}) => {
  return (
    <StyledList>
      {renderListItems(stations, activeStation, setActiveStation)}
    </StyledList>
  );
};

const mapStateToProps = state => ({
  ...state.widget
});

ListC.propTypes = {
  setActiveStation: PropTypes.func.isRequired,
  activeStation: PropTypes.number.isRequired,
  stations: PropTypes.array.isRequired
};

export default connect(mapStateToProps, {setActiveStation})(ListC);