import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Panel from './PanelC';

const StyledListItem = styled.li`
  color: #a2abbd;
  padding: 35px 0;
  border-bottom: 2px solid #3d3d46;
  font-size: 44px;

  &:last-child {
    border-bottom: 0;
  }
`;

const StyledFreq = styled.div`
  margin-left: auto;
  font-weight: 700;
`;

const StyledName = styled.div`
  margin-right: 15px;
`;

const StyledWrap = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`;



const ListItemC = ({station, active, onClick}) => {
  return (
    <StyledListItem>
      <Panel active={active}/>
      <StyledWrap onClick={onClick}>
        <StyledName>{station.name}</StyledName>
        <StyledFreq>{station.freq}</StyledFreq>
      </StyledWrap>
    </StyledListItem>
  );
};

ListItemC.propTypes = {
  station: PropTypes.object.isRequired,
  active: PropTypes.bool,
  onClick: PropTypes.func.isRequired
};

export default ListItemC;