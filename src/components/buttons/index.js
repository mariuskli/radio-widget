import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

export const Button = styled.button`
  width: 64px;
  height: 64px;
  background: transparent;
  border: 0;
  margin: 0;
  padding: 0;
  position: relative;
  outline: none;
  cursor: pointer;
`;

export const MinusButton = styled(Button)`
  border: 3px solid #a2abbd;
  border-radius: 50%;
  
  &:after {
    content: "";
    margin: auto;
    background: #a2abbd;
    position: absolute;
    width: 30px;
    height: 3px;
    border-radius: 2px;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
  }
`;

export const PlusButton = styled(MinusButton)`
  &:before {
    content: "";
    margin: auto;
    background: #a2abbd;
    position: absolute;
    width: 3px;
    height: 30px;
    border-radius: 2px;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
  }
`;

export const BackButton = styled(Button)`
  width: 34px;

  &:after, &:before {
    width: 34px;
    height: 4px;
    background: #fff;
    content: "";
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    position: absolute;
    border-radius: 2px;
  }

  &:after {
    transform: rotate(-45deg);
    margin-bottom: 41px;    
  }

  &:before {
    transform: rotate(45deg);
    margin-top: 41px;
  }
`;

const StyledQuitButton = styled(Button)`
  width: 44px;
  height: 44px;
  border: 4px solid #fff;
  border-radius: 50%;

  & > div {
    width: 4px;
    height: 20px;
    background: #fff;
    position: absolute;
    left: 0;
    right: 0;
    top: -10px;
    margin: 0 auto;
    border-radius: 2px;

    &:after, &:before {
      position: absolute;
      content: "";
      top: 0;
      height: 100%;
      width: 6px;
      background: ${props => props.gapColor || "#eaa455"};
    }

    &:after {
      left: 100%;
    }

    &:before {
      right: 100%;
    }
  }
`;

// Defining component as a function instead of const because of JavaScript function hoisting.
// Now We are able to to provide propTypes before component declaration. 
QuitButton.propTypes = {
  gapColor: PropTypes.string
};

export function QuitButton({gapColor}){
  return (
    <StyledQuitButton gapColor={gapColor}>
      <div></div>
    </StyledQuitButton>
  );
} 