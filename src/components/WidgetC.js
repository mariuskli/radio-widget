import React from 'react';
import styled from 'styled-components';

import Footer from './FooterC';
import Header from './HeaderC';
import List from './ListC';

const StyledWidget = styled.div`
  width: 640px;
  max-width: 100%;
  background: #272732;
  height: 1008px;
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  box-shadow: inset 0 2px 0 rgba(255, 255, 255, 0.2);
  border-radius: 50px;
`;

const WidgetC = () => {
  return (
    <StyledWidget>
      <Header />
      <List />
      <Footer />
    </StyledWidget>
  );
};

export default WidgetC;