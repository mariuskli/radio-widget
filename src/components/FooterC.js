import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const StyledFooter = styled.div`
  height: 160px;
  border-bottom-right-radius: 50px;
  border-bottom-left-radius: 50px;
  background: #22222b;
  border-top: 2px solid #4c4f5b;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const StyledOrangeText = styled.div`
  font-size: 22px;
  font-weight: 700;
  color: #edae61;
`;

const StyledName = styled.div`
  font-size: 44px;
  color: #a2abbd;
`;

const FooterC = ({activeStation}) => {
  return (
    <StyledFooter>
      { activeStation && <StyledOrangeText>CURRENTLY PLAYING</StyledOrangeText> }
      { activeStation && <StyledName>{activeStation.name}</StyledName> }
    </StyledFooter>
  );
};

const mapStateToProps = state => ({
  activeStation: state.widget.stations[state.widget.activeStation]
});

FooterC.propTypes = {
  activeStation: PropTypes.object
};  

export default connect(mapStateToProps)(FooterC);