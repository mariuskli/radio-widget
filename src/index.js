import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from './store';

import Widget from './components/WidgetC';
import './index.css';

ReactDOM.render(
  <Provider store={configureStore()}>
    <Widget />
  </Provider>,
  document.getElementById('root')
);